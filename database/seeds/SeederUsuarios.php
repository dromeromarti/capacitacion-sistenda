<?php

use Illuminate\Database\Seeder;
use App\User;
use Faker\Factory as Faker;

class SeederUsuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();

        $usuario = new User;
        $usuario->tipo_usuario_id = 1;
        $usuario->name ="Superusuario";
        $usuario->email= 'superusuario@correo.com';
        $usuario->telefono=$faker->tollFreePhoneNumber;
        $usuario->imagen='/persona.jpg';
        $usuario->password=bcrypt('password');
        $usuario->save();



        for($i=0;$i<10;$i++)
        {
            $faker = Faker::create();
            $usuario = new User;
            $usuario->tipo_usuario_id = rand(3, 4);
            $usuario->name = $faker->name;
            $usuario->email=$faker->email;
            $usuario->telefono=$faker->tollFreePhoneNumber;
            $usuario->imagen='/persona.jpg';
            $usuario->password=bcrypt('password');
            $usuario->save();
    	}

    }
}
