<?php

use Illuminate\Database\Seeder;
use App\Categoria;
use App\Subcategoria;
use App\Producto;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias = Categoria::all();

        foreach($categorias as $categoria) {
            foreach($categoria->subcategorias as $subcategoria) {
                for($i = 1; $i <= 10; $i++) {
                    $objeto = new Producto;
                    $objeto->nombre = "Producto #$i de $subcategoria->nombre";
                    $objeto->descripcion = 'Lorem Ipsum';
                    $objeto->precio = rand(100, 500);
                    $objeto->uri = 'https://images.unsplash.com/photo-1593642634443-44adaa06623a?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=625&q=80';
                    $subcategoria->productos()->save($objeto);
                }
            }
        }
    }
}
