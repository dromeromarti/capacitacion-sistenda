<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TiposUsuarioSeeder::class);
        $this->call(SeederUsuarios::class);
        $this->call(CategoriasSeeder::class);
        $this->call(SubcategoriasSeeder::class);
        $this->call(ProductosSeeder::class);
    }
}
