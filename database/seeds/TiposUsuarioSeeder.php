<?php

use Illuminate\Database\Seeder;
use App\TipoUsuario;

class TiposUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objeto = new TipoUsuario;
        $objeto->nombre = 'Superusuario';
        $objeto->save();

        $objeto = new TipoUsuario;
        $objeto->nombre = 'Administrador';
        $objeto->save();

        $objeto = new TipoUsuario;
        $objeto->nombre = 'Vendedor';
        $objeto->save();

        $objeto = new TipoUsuario;
        $objeto->nombre = 'Cliente';
        $objeto->save();
    }
}
