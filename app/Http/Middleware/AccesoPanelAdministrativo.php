<?php

namespace App\Http\Middleware;
use Auth;

use Closure;

class AccesoPanelAdministrativo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->tipoUsuario->id != 1 && Auth::user()->tipoUsuario->id != 2) {
            return redirect()->route('index');
        }

        return $next($request);
    }
}
