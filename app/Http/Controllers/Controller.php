<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\User;
use App\Categoria;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function prueba()
    {
        // Comentario de prueba para push daniel
    	$prueba="Esto es una variable de prueba";
    	return view('prueba')->with('prueba',$prueba);
    }

    public function index()
    {
    	$usuarios=User::orderBy('name','ASC')->paginate(8);
        $categorias = Categoria::with('subcategorias.productos')->get();
        //dd($categorias);
    	/*$prueba="Esto es una variable de prueba";
    	dd($usuarios,$prueba);*/
    	return view('welcome')->with('usuarios',$usuarios)->with('categorias',$categorias);
    }
}
