<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subcategoria;
use App\Categoria;
use App\Producto;
use Validator;
use Session;
use App\Traits\TraitNotificaciones;
use App\Traits\TraitImagenes;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;



class ProductosController extends Controller
{
    use TraitNotificaciones, TraitImagenes;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $subcategoria = 2;
        $subcategoria = Subcategoria::find($subcategoria);

        $categoria = Categoria::find($subcategoria->categoria_id);
        $coleccion = $subcategoria->productos()->withTrashed()->orderBy('created_at','DESC')->get();

        return view('panel-admin.productos.index')
            ->with('categoria', $categoria->id)
            ->with('subcategoria',$subcategoria->id)
            ->with('coleccion', $coleccion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$categoria,$subcategoria)
    {
        //

         $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'precio' => 'required|string|max:255',
            'descripcion' => 'required|string',
            'imagen' => 'nullable|image',
        ]);

        if ($validator->fails()) {
           $this->notificacionError();

            return redirect()->route('categorias.subcategorias.productos.create',[$categoria],[$subcategoria])
                ->withErrors($validator)
                ->withInput();
        }

        $objeto = new Producto;
        $objeto->subcategoria_id = $subcategoria;
        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->descripcion=$request->descripcion;
        $objeto->uri = (isset($request->imagen) && !empty($request->imagen)) ? $this->guardarImagen($request->imagen) : null;
        $objeto->precio=filter_var($request->precio, FILTER_SANITIZE_STRING);
        $objeto->save();

       // $this->notificacionSuccess();

        return redirect()->route('categorias.subcategorias.productos.index', [$categoria,$subcategoria]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($categoria,$subcategoria,$id)
    {
        //
        $objeto = Producto::find($id);

        return view('panel-admin.productos.edit')
            ->with('categoria', $categoria)
            ->with('subcategoria',$subcategoria)
            ->with('objeto', $objeto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $categoria, $subcategoria, $id)
    {
        //

         $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'precio' => 'required|string|max:255',
            'descripcion' => 'required|string',
            'uri' => 'nullable|string|max:255',
        ]);

        if ($validator->fails()) {
           $this->notificacionError();

            return redirect()->route('categorias.subcategorias.productos.edit',[$categoria],[$subcategoria])
                ->withErrors($validator)
                ->withInput();
        }

        $objeto = Producto::find($id);

        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->descripcion=$request->descripcion;
        $objeto->precio=filter_var($request->precio, FILTER_SANITIZE_STRING);
        $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
        $objeto->save();

       // $this->notificacionSuccess();

        return redirect()->route('categorias.subcategorias.productos.index', [$categoria,$subcategoria]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoria,$subcategoria,$id)
    {
        //
        $objeto = Producto::withTrashed()->find($id);

        if($objeto->deleted_at) {
            $objeto->deleted_at = null;
            $objeto->save();
           // $this->notificacionSuccessReactivacion();
        } else {
            $objeto->delete();
            //$this->notificacionSuccessEliminacion();
        }

        return redirect()->route('categorias.subcategorias.productos.index', [$categoria,$subcategoria]);

    }

     private function formatoImagen($imagen)
    {
        $img = Image::make($imagen);
        $img->resize(700, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img = $img->stream()->__toString();
        return $img;
    }


    public function ejemplosInnerJoin()
    {




    }











}
