<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Response;

use Validator;
use App\Categoria;
use App\Subcategoria;
use App\Producto;

class ProductosApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $productos = Productos::all();
      return $productos;
    }

    public function productosDe($id){
      $productos = Producto::where('subcategoria_id',$id)->get();
      return $productos;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'nombre' => 'required|string|max:255',
          'precio' => 'required|numeric',
          'descripcion' =>  'required|string|max:255',
          'uri' => 'nullable|string|max:255',
          'subcategoria_id' =>  'required|integer'
      ]);

      if ($validator->fails()) {
          $response = [
              'res' => false,
              'errores' => $validator->errors()
          ];

          return Response::json($response, 200);
      }

      $objeto = new Producto;
      $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
      $objeto->descripcion = filter_var($request->descripcion, FILTER_SANITIZE_STRING);
      $objeto->subcategoria_id = $request->subcategoria_id;
      $objeto->precio = $request->precio;
      $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
      $objeto->save();

      $response = [
          'res' => true,
      ];

      return Response::json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $coleccion = Producto::withTrashed()->where('id',$id)->first();

      return Response::json($coleccion, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(), [
          'nombre' => 'required|string|max:255',
          'precio' => 'required|numeric',
          'descripcion' =>  'required|string|max:255',
          'uri' => 'nullable|string|max:255',
          'subcategoria_id' =>  'required|integer'
      ]);

      if ($validator->fails()) {
          $response = [
              'res' => false,
              'errores' => $validator->errors()
          ];

          return Response::json($response, 200);
      }

      $objeto = Producto::find($id);
      $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
      $objeto->descripcion = filter_var($request->descripcion, FILTER_SANITIZE_STRING);
      $objeto->precio = $request->precio;
      $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
      $objeto->save();

      $response = [
          'res' => true,
      ];

      return Response::json($response, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objeto = Producto::withTrashed()->find($id);

        if($objeto->deleted_at) {
            $objeto->deleted_at = null;
            $objeto->save();
        } else {
            $objeto->delete();
        }
        $response = [
            'res' => true
        ];
        return Response::json($response,200);
    }
}
