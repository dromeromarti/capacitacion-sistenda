<?php

namespace App\Http\Controllers;

use App\Traits\TraitNotificaciones;

use Illuminate\Http\Request;
use Validator;
use Session;
use App\Categoria;

class CategoriasController extends Controller
{
    use TraitNotificaciones;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coleccion = Categoria::withTrashed()->get();

        return view('panel-admin.categorias.index')
            ->with('coleccion', $coleccion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel-admin.categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'uri' => 'nullable|string|max:255',
        ]);

        if ($validator->fails()) {
           $this->notificacionError();

            return redirect()->route('categorias.create')
                ->withErrors($validator)
                ->withInput();
        }

        $objeto = new Categoria;
        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
        $objeto->save();

        $this->notificacionSuccess();

        return redirect()->route('categorias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objeto = Categoria::find($id);

        return view('panel-admin.categorias.edit')
            ->with('objeto', $objeto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'uri' => 'nullable|string|max:255',
        ]);

        if ($validator->fails()) {
           $this->notificacionError();

            return redirect()->route('categorias.edit', [$id])
                ->withErrors($validator)
                ->withInput();
        }

        $objeto = Categoria::find($id);
        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : $objeto->uri;
        $objeto->save();

        $this->notificacionSuccessActualizacion();

        return redirect()->route('categorias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objeto = Categoria::withTrashed()->find($id);

        if($objeto->deleted_at) {
            $objeto->deleted_at = null;
            $objeto->save();
            $this->notificacionSuccessReactivacion();
        } else {
            $objeto->delete();
            $this->notificacionSuccessEliminacion();
        }

        return redirect()->route('categorias.index');
    }
}
