<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;

use Illuminate\Http\Request;
use Validator;
use App\Categoria;

class CategoriasAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coleccion = Categoria::withTrashed()->get();

        return Response::json($coleccion, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'uri' => 'nullable|string|max:255',
        ]);

        if ($validator->fails()) {
            $response = [
                'res' => false,
                'errores' => $validator->errors()
            ];

            return Response::json($response, 200);
        }

        $objeto = new Categoria;
        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
        $objeto->save();

        $response = [
            'res' => true,
        ];

        return Response::json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $objeto = Categoria::find($id);
      return Response::json($objeto, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(), [
          'nombre' => 'required|string|max:255',
          'uri' => 'nullable|string|max:255',
      ]);

      if ($validator->fails()) {
          $response = [
              'res' => false,
              'errores' => $validator->errors()
          ];

          return Response::json($response, 200);
      }

      $objeto = Categoria::find($id);
      if($objeto){
        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
        $objeto->save();
        $response = [
            'res' => true,
        ];
        return Response::json($response, 201);
      }
      else{
        $response = [
            'res' => false,
            'errores' => ["Error al obtener la categoria"]
        ];
        return Response::json($response,200);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $objeto = Categoria::withTrashed()->find($id);

      if($objeto->deleted_at) {
          $objeto->deleted_at = null;
          $objeto->save();
      } else {
          $objeto->delete();
      }
      $response = [
          'res' => true
      ];
      return Response::json($response,200);

    }
}
