<?php

namespace App\Http\Controllers;

use App\Traits\TraitNotificaciones;

use Illuminate\Http\Request;
use Validator;
use Session;
use App\Categoria;
use App\Subcategoria;

class SubcategoriasController extends Controller
{
    use TraitNotificaciones;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoria = 1;
        $categoria = Categoria::find($categoria);
        $coleccion = $categoria->subcategorias()->withTrashed()->get();

        return view('panel-admin.subcategorias.index')
            ->with('categoria', $categoria->id)
            ->with('coleccion', $coleccion);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoria)
    {
        $categoriaProvisional=Categoria::find($categoria);
        return view('panel-admin.subcategorias.create')
            ->with('categoria', $categoria)->with('categoriaProvisional',$categoriaProvisional);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $categoria)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'uri' => 'nullable|string|max:255',
        ]);

        if ($validator->fails()) {
           $this->notificacionError();

            return redirect()->route('categorias.subcategorias.create',[$categoria])
                ->withErrors($validator)
                ->withInput();
        }

        $objeto = new Subcategoria;
        $objeto->categoria_id = $categoria;
        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
        $objeto->save();

        $this->notificacionSuccess();

        return redirect()->route('categorias.subcategorias.index', [$categoria]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($categoria, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($categoria, $id)
    {
        $objeto = Subcategoria::find($id);

        return view('panel-admin.subcategorias.edit')
            ->with('categoria', $categoria)
            ->with('objeto', $objeto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $categoria, $id)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'uri' => 'nullable|string|max:255',
        ]);

        if ($validator->fails()) {
           $this->notificacionError();

            return redirect()->route('categorias.create')
                ->withErrors($validator)
                ->withInput();
        }

        $objeto = Subcategoria::find($id);
        $objeto->nombre = filter_var($request->nombre, FILTER_SANITIZE_STRING);
        $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
        $objeto->save();

        $this->notificacionSuccess();

        return redirect()->route('categorias.subcategorias.index', [$categoria]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoria, $id)
    {
        $objeto = Subcategoria::withTrashed()->find($id);

        if($objeto->deleted_at) {
            $objeto->deleted_at = null;
            $objeto->save();
            $this->notificacionSuccessReactivacion();
        } else {
            $objeto->delete();
            $this->notificacionSuccessEliminacion();
        }

        return redirect()->route('categorias.subcategorias.index', [$categoria]);
    }
}
