<?php

namespace App\Http\Controllers;

use App\InformacionPago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use Session;

class InformacionPagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'venta_id' => 'integer|required',
        'id'  => 'string|max:255'
      ]);

      if ($validator->fails()) {
          $response = [
              'res' => false,
              'errores' => $validator->errors()
          ];

          return Response::json($response, 200);
      }

      $objeto = new InformacionPago;
      $objeto->referencia = $request->id;
      $objeto->venta_id = $request->venta_id;
      $objeto->tipo_pago_id = $request->tipo_pago_id;
      $objeto->save();

      $response = [
          'res' => true,
          'id'  => $objeto->id
      ];

      return Response::json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InformacionPago  $informacionPago
     * @return \Illuminate\Http\Response
     */
    public function show(InformacionPago $informacionPago)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InformacionPago  $informacionPago
     * @return \Illuminate\Http\Response
     */
    public function edit(InformacionPago $informacionPago)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InformacionPago  $informacionPago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InformacionPago $informacionPago)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InformacionPago  $informacionPago
     * @return \Illuminate\Http\Response
     */
    public function destroy(InformacionPago $informacionPago)
    {
        //
    }
}
