<?php

namespace App\Http\Controllers;

use App\DetalleVenta;
use Illuminate\Http\Request;
use App\Venta;
use Illuminate\Support\Facades\Response;
use Validator;
use Session;

class DetalleVentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
        "venta_id" => 'integer|required',
        "id" => 'integer|required',
        'cantidad' => 'integer|required'
      ]);

      if ($validator->fails()) {
          $response = [
              'res' => false,
              'errores' => $validator->errors()
          ];

          return Response::json($response, 200);
      }

      $objeto = new DetalleVenta;
      $objeto->venta_id = $request->venta_id;
      $objeto->producto_id = $request->id;
      $objeto->cantidad = $request->cantidad;
      // // $objeto->total
      // $objeto->fecha_compra = Carbon::now();
      // $objeto->precio = $request->precio;
      // $objeto->uri = isset($request->uri) && !empty($request->uri) ? filter_var($request->uri, FILTER_SANITIZE_STRING) : null;
      $objeto->save();

      $response = [
          'res' => true,
          'id'  => $objeto->id
      ];

      return Response::json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleVenta  $detalleVenta
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleVenta $detalleVenta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleVenta  $detalleVenta
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleVenta $detalleVenta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleVenta  $detalleVenta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetalleVenta $detalleVenta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleVenta  $detalleVenta
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetalleVenta $detalleVenta)
    {
        //
    }
}
