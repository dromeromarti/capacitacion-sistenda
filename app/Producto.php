<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use Config;

class Producto extends Model
{
    use SoftDeletes;

    public function subcategoria() {
        return $this->belongsTo(Subcategoria::class);
    }

    public function url() {
        $s3 = Storage::disk('s3');
        $cliente = $s3->getDriver()->getAdapter()->getClient();
        $expiracion = '+15 seconds';
        $comando = $cliente->getCommand('GetObject', [
            'Bucket' => Config::get('filesystems.disks.s3.bucket'),
            'Key' => $this->uri
        ]);
        $res = $cliente->createPresignedRequest($comando, $expiracion);
        return (string) $res->getUri();
    }

    public function estaEnDescuento() {
        return true;
    }
}
