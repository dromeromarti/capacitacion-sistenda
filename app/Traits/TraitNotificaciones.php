<?php
namespace App\Traits;

use Session;

trait TraitNotificaciones {

    public function notificacionSuccess() {

        Session::flash('clase', 'success');
        Session::flash('mensaje', 'El registro se guardó de manera exitosa.');

        return;
    }

    public function notificacionSuccessActualizacion() {

        Session::flash('clase', 'success');
        Session::flash('mensaje', 'El registro se editó de manera exitosa.');

        return;
    }

    public function notificacionSuccessEliminacion() {

        Session::flash('clase', 'warning');
        Session::flash('mensaje', 'El registro se eliminó de manera exitosa.');

        return;
    }

    public function notificacionSuccessReactivacion() {

        Session::flash('clase', 'info');
        Session::flash('mensaje', 'El registro se reactivó de manera exitosa.');

        return;
    }

    public function notificacionError() {

        Session::flash('clase', 'danger');
        Session::flash('mensaje', 'Hubo un error, por favor revisa el formulario para más detalles.');

        return;
    }

    // public function notificacionUpdateSuccess() {

    //     Session::flash('alert', 'alert-success');
    //     Session::flash('mensaje', 'El registro se modifico correctamente.');

    //     return;
    // }

    // public function notificacionDestroySuccess() {

    //     Session::flash('alert', 'alert-success');
    //     Session::flash('mensaje', 'El registro se elimino correctamente.');

    //     return;
    // }

    // public function notificacionReactiveSuccess() {

    //     Session::flash('alert', 'alert-success');
    //     Session::flash('mensaje', 'El registro se reactivo correctamente.');

    //     return;
    // }



}
