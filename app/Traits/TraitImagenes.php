<?php

namespace App\Traits;

use Illuminate\Http\Request;
use App\Traits\TraitImagenes;

use App\Producto;
use App\Categoria;
use App\Subcategoria;
use App\Imagen;
use Image;
use Session;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


trait TraitImagenes
{
    private function _formatoImagen($imagen)
    {
        $img = Image::make($imagen);
        $img->resize(null, 800, function ($constraint) {
            $constraint->aspectRatio();
        });
        $img = $img->stream()->__toString();
        return $img;
    }

    public function guardarImagen($imagen){
        $id = Str::orderedUuid();
        $url = Storage::disk('s3')->put($id,$this->_formatoImagen($imagen),'private');
        return $id;
    }

}
