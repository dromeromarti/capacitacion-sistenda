<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategoria extends Model
{
    use SoftDeletes;

    public function categoria() {
        return $this->belongsTo(Categoria::class);
    }

    public function productos() {
        return $this->hasMany(Producto::class);
    }
}
