<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('categorias', 'CategoriasAPIController');
Route::get('subcategorias/subcategorias-de/{id}','SubcategoriasAPIController@subcategoriasDe');
Route::resource('subcategorias', 'SubcategoriasAPIController');
Route::resource('productos','ProductosApiController');
Route::get('productos/productos-de/{id}','ProductosApiController@productosDe');
Route::resource('ventas','VentaController');
Route::resource('detalle-venta','DetalleVentaController');
Route::resource('detalle-pago','InformacionPagoController');
