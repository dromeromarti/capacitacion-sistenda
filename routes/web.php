<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Controller@index')->name('index');
Route::get('/prueba','Controller@prueba');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cerrarSesion', 'HomeController@cerrarSesion')->name('cerrarSesion');

Route::prefix('panel-admin')->middleware(['auth', 'accesoPanelAdministrativo'])->group(function () {
    Route::resource('categorias', 'CategoriasController');
    // Route::get('categorias', 'CategoriasController@index')->name('categorias.index');
    // Route::get('categorias/create', 'CategoriasController@create')->name('categorias.create');
    // Route::post('categorias', 'CategoriasController@store')->name('categorias.store');
    // Route::get('categorias/{id}', 'CategoriasConroller@show')->name('categorias.show');
    // Route::get('categorias/{id}/edit', 'CategoriasController@edit')->name('categorias.edit');
    // Route::put('categorias/{id}', 'CategoriasController@update')->name('categorias.update');
    // Route::delete('categorias/{id}', 'CategoriasController@destroy')->name('categorias.destroy');
    Route::resource('categorias.subcategorias', 'SubcategoriasController')->parameters(['categorias' => 'categorias:id']);
    Route::resource('categorias.subcategorias.productos', 'ProductosController');
});
