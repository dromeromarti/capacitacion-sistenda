@extends('layouts.auth')

@section('contenido')
<div class="col-md-4 col-sm-6 ml-auto mr-auto">
    <form class="form" method="POST" action="{{route('login')}}">
        @csrf
        <div class="card card-login card-hidden">
            <div class="card-header ">
                <h3 class="header text-center">Login</h3>
            </div>
            <div class="card-body ">
                <div class="card-body">
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" name="email" placeholder="Enter email" class="form-control @error('email') is-invalid @enderror">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="" checked>
                                <span class="form-check-sign"></span>
                                Subscribe to newsletter
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-warning btn-wd">Login</button>
            </div>
        </div>
    </form>
</div>
@endsection
