@extends('layouts.admin')

@section('contenido')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/home">Panel</a></li>
    <li class="breadcrumb-item active" aria-current="page">Administración Categorias</li>
  </ol>
</nav>




<div class="card data-tables">
    <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
        <div class="toolbar">
            <!--        Here you can write extra buttons/actions for the toolbar              -->
        </div>
        <div class="row m-5">
            <div class="col-md-4">
                <a href="{{route('categorias.create')}}" class="btn btn-primary btn-block">Agregar</a>
            </div>
        </div>
        <div class="fresh-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Subcategorías</th>
                        <th class="disabled-sorting text-right">Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Subcategorías</th>
                        <th class="disabled-sorting text-right">Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($coleccion as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->nombre}}</td>
                            <td>
                                <a href="/panel-admin/categorias/{{$item->id}}/subcategorias" class="btn btn-primary btn-block">Admn. Subcategorías</a>
                            </td>
                            <td class="text-right">
                                <a href="{{route('categorias.edit', [$item->id])}}" class="btn btn-link btn-warning edit"><i class="fa fa-edit"></i></a>
                                <form method="POST" action="{{route('categorias.destroy', [$item->id])}}">
                                    @csrf
                                    @method('DELETE')
                                    @if ($item->deleted_at)
                                        <input type="submit" class="btn btn-info" value="Reactivar">
                                    @else
                                        <input type="submit" class="btn btn-danger" value="Borrar">
                                    @endif

                                </form>
                                {{-- <a href="#" class="btn btn-link btn-danger remove"><i class="fa fa-times"></i></a> --}}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

