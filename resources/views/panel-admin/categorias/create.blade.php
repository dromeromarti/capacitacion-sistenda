@extends('layouts.admin')

@section('contenido')
<div class="card stacked-form">
    <div class="card-header ">
        <h4 class="card-title">Agregar</h4>
    </div>
    <form method="POST" action="{{route('categorias.store')}}">
        @csrf
        <div class="card-body ">
            <div class="form-group">
                <label>Nombre</label>
                <input type="text" name="nombre" class="form-control @error('nombre') is-invalid @enderror"
                    value="{{old('nombre')}}"
                    placeholder="Nombre..."
                    required>
                @error('nombre')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label>URI</label>
                <input type="text" name="uri" class="form-control @error('uri') is-invalid @enderror"
                    value="{{old('uri')}}"
                    placeholder="URI...">
                @error('uri')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="card-footer ">
            <button type="submit" class="btn btn-fill btn-info">Guardar</button>
        </div>
    </form>
</div>
@endsection
