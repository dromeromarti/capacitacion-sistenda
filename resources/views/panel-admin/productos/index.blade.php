@extends('layouts.admin')

@section('contenido')


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/home">Panel</a></li>
    <li class="breadcrumb-item"><a href="/panel-admin/categorias">Administración Categorias</a></li>
    <li class="breadcrumb-item"><a href="/panel-admin/categorias/{{$categoria}}/subcategorias">Administración Subcategorias</a></li>
    <li class="breadcrumb-item active" aria-current="page">Administración Productos</li>

  </ol>
</nav>


<div class="card data-tables">
    <div class="card-body table-striped table-no-bordered table-hover dataTable dtr-inline table-full-width">
        <div class="toolbar">
            <!--        Here you can write extra buttons/actions for the toolbar              -->
        </div>
        <div class="row m-5">
            <div class="col-md-4">
                <a href="#" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary btn-block">Agregar</a>
            </div>
        </div>
        <div class="fresh-datatables">
            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0"
                width="100%" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Descripcion</th>
                        <th class="disabled-sorting text-right">Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Imagen</th>
                        <th>Nombre</th>
                        <th>Productos</th>
                        <th class="disabled-sorting text-right">Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($coleccion as $item)
                    <tr>
                        <td>{{$item->id}}</td>
                        <td><img src="{{$item->url()}}" class="img-fluid"></td>
                        <td>{{$item->nombre}}</td>
                        <td>${{number_format($item->precio,2)}}</td>
                        <td>
                            @php
                            echo $item->descripcion;
                            @endphp
                        </td>

                        <td class="text-right">
                            <a href="{{route('categorias.subcategorias.productos.edit', [$item->subcategoria->categoria->id, $item->subcategoria->id, $item->id])}}" class="btn btn-link btn-warning edit"><i
                                    class="fa fa-edit"></i></a>
                            <form method="POST" action="{{route('categorias.subcategorias.productos.destroy', [$item->subcategoria->categoria, $item->subcategoria, $item])}}" onsubmit="return confirm('¿Estas seguro que quieres eliminar a {{$item->nombre}}?');">
                                @csrf
                                @method('DELETE')
                                @if ($item->deleted_at)
                                <input type="submit" class="btn btn-info" value="Reactivar">
                                @else
                                <input type="submit" class="btn btn-danger" value="Borrar">
                                @endif

                            </form>
                            {{-- <a href="#" class="btn btn-link btn-danger remove"><i class="fa fa-times"></i></a> --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>





<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Insertar Producto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form method="POST" action="{{route('categorias.subcategorias.productos.store', [$item->subcategoria->categoria, $item->subcategoria, $item])}}" enctype='multipart/form-data'>
        @csrf
        <div class="card-body ">
            <div class="form-group">
                <label>Nombre</label>
                <input type="text" name="nombre" class="form-control @error('nombre') is-invalid @enderror"
                    value="{{old('nombre')}}" placeholder="Nombre..." required>
                @error('nombre')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <label>Precio</label>
                <input type="number" step="any" name="precio" class="form-control @error('precio') is-invalid @enderror"
                    value="{{old('precio')}}" placeholder="Precio..." required>
                @error('precio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <textarea name="descripcion" style="height:60px;" class="form-control @error('descripcion') is-invalid @enderror"
                     placeholder="Descripcion..." required>{{old('descripcion')}}</textarea>
                @error('precio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>


             <div class="form-group">
          <label>Imagen de Perfil  (1900x1200):</label><br>
             <div class="btn btn-primary">
                Seleccionar Imagen
                <input required id="uploadImage" accept="image/*"  type="file" name="imagen" onchange="PreviewImage();">

                  </div>
            @if($errors->has('imagen'))
            <span class="help-block text-danger">
                {{ $errors->first('imagen') }}
            </span>
            @endif
                  <img id="uploadPreview" class="img-fluid img-responsive" style="" />
        </div>
        </div>
        <div class="card-footer ">
            <button type="submit" class="btn btn-fill btn-info">Guardar</button>
        </div>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


<script src="//cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'descripcion' );

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>

@endsection
