@extends('layouts.admin')

@section('contenido')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/home">Panel</a></li>
    <li class="breadcrumb-item"><a href="/panel-admin/categorias">Administración Categorias</a></li>
    <li class="breadcrumb-item active" aria-current="page">Administración Subcategorias</li>

  </ol>
</nav>

<div class="card stacked-form">
    <div class="card-header ">
        <h4 class="card-title">Editar</h4>
    </div>
    <form method="POST" action="{{route('categorias.subcategorias.productos.update', [$categoria,$subcategoria,$objeto->id])}}">
        @csrf
        @method('PUT')
        <div class="card-body ">
            <div class="form-group">
                <label>Nombre</label>
                <input type="text" name="nombre" class="form-control @error('nombre') is-invalid @enderror"
                    value="{{old('nombre', $objeto->nombre)}}" placeholder="Nombre..." required>
                @error('nombre')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

             <div class="form-group">
                <label>Precio</label>
                <input type="number" step="any" name="precio" class="form-control @error('precio') is-invalid @enderror"
                    value="{{$objeto->precio}}" placeholder="Precio..." required>
                @error('precio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <label>Descripcion</label>
                <textarea name="descripcion" style="height:60px;" class="form-control @error('descripcion') is-invalid @enderror"
                     placeholder="Descripcion..." required>{{$objeto->descripcion}}</textarea>  
                @error('precio')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>


            <div class="form-group">
                <label>URI</label>
                <img src="{{$objeto->uri}}" class="img-fluid">
                @error('uri')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="card-footer ">
            <button type="submit" class="btn btn-fill btn-info">Guardar</button>
        </div>
    </form>
</div>

<script src="//cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'descripcion' );
</script>
@endsection
